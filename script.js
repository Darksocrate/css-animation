let particles = [];
function setup() {
  createCanvas(450, 450);
  angleMode(DEGREES);
  translate(225, 225);
  noStroke();
  for (let i = 40; i >= 0; i--) {
    let r = i * random(5, 6) + 50;
    let angle = random(0, 360);
    for (let j = 50; j >= 0; j--) {
      r -= 0.1;
      angle += random(2, 9);
      particles.push(new Particle(r, angle, random(1, 5)));
    }
  }
  frameRate(30);
}
function draw() {
  background(0);
  translate(200, 200);
  let r = 210;
  for (let i = particles.length - 1; i >= 0; i--) {
    if (particles[i].r < random(20, 25)) {
      particles[i].r = r;
    } else {
      particles[i].update();
      particles[i].show();
    }
  }
}
function Particle(r, theta, size) {
  this.r = r;
  this.theta = theta;
  this.size = size;
  this.update = function() {
    this.r -= random(0.5, 0.8) / Math.log10(this.r * 2);
    this.theta += random(0.9, 1.5) / Math.log10(this.r / 5);
  };
  this.show = function() {
    fill(255, Math.min(2 * this.r, 255));
    ellipse(this.r * sin(this.theta), this.r * cos(this.theta), this.size);
  };
}